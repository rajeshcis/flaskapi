from flask import Flask, request
from flask_restful import Resource, Api

import requests


app = Flask(__name__)
api = Api(app)

class UnisportApi(object):
    """This class is used for load all sample data from given URL and load into self.products attribute"""

    def __init__(self):
        self.url = 'https://www.unisport.dk/api/sample/'
        self.products = []

    def get_all_products(self, order_by=True):
        """ method for fetching all the product data from 'https://www.unisport.dk/api/sample/' """
        response = requests.get(self.url)
        if response.status_code == 200:
            products = response.json()['products']
            """ soting the product data by price lower to higher """
            if order_by:
                self.products = sorted(products, key=lambda k: float(k['price'].replace(',', '.')))
            else:
                self.products = products



class ProductList(Resource, UnisportApi):
    """ class used for displaying the product list with pagination """

    def get(self):
        """ method to return the product list with pagination 10 record per page """

        page = request.args.get('page', type=int, default=1)
        self.get_all_products()  # This is used for load all sample data from url and  ordered with the cheapest first
        #paginate the prodcut data according the page value, per page records is 10 
        return self.products[((page-1)*10):(page * 10)]



class ProductListKids(Resource, UnisportApi):
    """ class used for the products where kids=1 ordered with the cheapest first """

    def get(self):
        """ method used to return the products where kids=1 ordered with the cheapest first """

        self.get_all_products()  # This is used for load all sample data from url and  ordered with the cheapest first
        # filtering the product where kids field value is 1
        return filter(lambda a: int(a['kids']) == 1,  self.products)
    

class ProductDetail(Resource, UnisportApi):
    """ class used for details of individual product """

    def get(self, product_id):
        """ method is used to return the details of individual product """

        self.get_all_products(order_by=False)  # This is used for load all sample data from url
 
        # finding the product for requested product id 

        for product in self.products:
            if product['id'] == product_id:
                return product
        return {'Data': 'There is no data for this product id %s' %(product_id)}

##
## Actually setup the Api resource routing here
##
api.add_resource(ProductDetail, '/products/<product_id>/')
api.add_resource(ProductList, '/products/')
api.add_resource(ProductListKids, '/products/kids/')

if __name__ == '__main__':
    app.run(debug=True) 